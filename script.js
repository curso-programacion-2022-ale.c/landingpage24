// document.querySelector("boton").setAttribute("onclick", "validar()");
// let user = document.querySelector("#nombre");
// let email = document.querySelector("email");
// let mens = document.querySelector("#captcha");


// function validar(){

// }


document.addEventListener('DOMContentLoaded', function () {
    let user = document.querySelector("#nombre");
    let email = document.querySelector("#email");
    let captchaInput = document.querySelector("#captcha");
    let boton = document.querySelector("#boton");
    let captchaNumbers = document.querySelector("#captcha-numbers");
    let mensaje = document.createElement('p');
    mensaje.id = 'mensaje-validacion';

    // Generar un captcha aleatorio al cargar la página
    let num1 = Math.floor(Math.random() * 10) + 1;
    let num2 = Math.floor(Math.random() * 10) + 1;
    captchaNumbers.textContent = `${num1} + ${num2} = ?`;

    // Añadir el evento de click al botón
    boton.addEventListener("click", validar);

    function validar() {
        // Limpiar cualquier mensaje previo
        let mensajePrevio = document.querySelector('#mensaje-validacion');
        if (mensajePrevio) {
            mensajePrevio.remove();
        }

        // Validar campo de nombre
        if (user.value.trim() === '') {
            mensaje.textContent = 'Por favor, ingrese su nombre completo.';
            mensaje.style.color = 'red';
            document.querySelector('.form').appendChild(mensaje);
            return false;
        }

        // Validar campo de correo electrónico
        if (email.value.trim() === '') {
            mensaje.textContent = 'Por favor, ingrese su correo electrónico.';
            mensaje.style.color = 'red';
            document.querySelector('.form').appendChild(mensaje);
            return false;
        }

        // Validar formato del correo electrónico usando una expresión regular
        let regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!regexEmail.test(email.value.trim())) {
            mensaje.textContent = 'Por favor, ingrese un correo electrónico válido.';
            mensaje.style.color = 'red';
            document.querySelector('.form').appendChild(mensaje);
            return false;
        }

        // Validar el captcha
        let captchaResult = num1 + num2;
        if (parseInt(captchaInput.value.trim()) !== captchaResult) {
            mensaje.textContent = 'La respuesta al captcha es incorrecta. Por favor, inténtelo de nuevo.';
            mensaje.style.color = 'red';
            document.querySelector('.form').appendChild(mensaje);
            return false;
        }

        // Si todas las validaciones son exitosas, mostrar mensaje de éxito
        mensaje.textContent = '¡Gracias por suscribirte!';
        mensaje.style.color = 'green';
        document.querySelector('.form').appendChild(mensaje);

        // Limpiar el formulario
        user.value = '';
        email.value = '';
        captchaInput.value = '';

        return true;
    }
});


// El motivo por el cual se eliminó onclick="validar()" del HTML y se reemplazó con addEventListener 
// en el JavaScript es para seguir las mejores prácticas de desarrollo moderno. Aquí te explico las razones:

